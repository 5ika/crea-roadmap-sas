# Roadmap - Cours Serveur, Architecture et Sécurité

Cette roadmap liste ce qu'il est nécessaire de connaître et de comprendre pour être à l'aise en tant que développeur dans le monde moderne. Ce n'est pas une liste exhaustive de tout ce qu'il y a dans les cours de SAS. Uniquement les concepts importants à maîtriser.

L'objectif est que chacun puisse voir où il ou elle en est dans ses connaissances et cibler ce qu'il faut approfondir.
Il existe énormément de ressources sur le Web pour creuser tous les sujets. J'en ai sélectionné quelques uns pour commencer mais il est nécessaire d'aller plus loin.
Le cours est dans tous les cas la première chose à reprendre.

Vous pouvez *forker* ce dépôt pour récupérer la liste et la remplir en fonction de votre avancée.
Il est également possible de proposer des ajouts à cette page pour en faire profiter les autres en faisant des *merge requests* ou en ouvrant une issue dans ce dépôt.

## Le terminal

- [ ] Qu'est-ce qu'est un shell et que peut-on faire avec
- [ ] Les commandes pour se déplacer et gérer des fichiers simples: `ls, cd, mv, cp, rm, echo, cat`
- [ ] La hiérarchie de fichier d'un système d'exploitation Unix / Linux 
- [ ] Effectuer des actions en tant qu'administrateur (`sudo`)
- [ ] La gestion des droits et permissions sur un système Unix / Linux (`chmod, chown`)
- [ ] Le concept Keep It Simple Stupid
- [ ] Trouver de l'aide sur l'utilisation d'une commande (`help, man, -h`)

### Ressources

- [L'art de la ligne de commande](https://github.com/jlevy/the-art-of-command-line/blob/master/README-fr.md)
- [Tuto OpenClassrooms](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux)
- [A guide to learn Bash](https://github.com/Idnan/bash-guide)
- [Google Shell style guide](https://google.github.io/styleguide/shellguide.html)
- https://github.com/alebcay/awesome-shell
- https://github.com/awesome-lists/awesome-bash

### Pour aller plus loin

- Découvrir d'autres shells plus avancés que Bash
- Développer son propre shell pour faciliter son travail

## Le Bash scripting

- [ ] Exécuter un ensemble de commande shell dans un script
- [ ] Utiliser les concepts standards avec Bash: conditions, boucles, tableaux, 
- [ ] Utiliser des arguments dans un script
- [ ] Les variables d'environnement
- [ ] Les codes de retour (`exit`)

### Ressources

- [Livre Bash Shell Scripting](https://en.wikibooks.org/wiki/Bash_Shell_Scripting)
- [Tuto OpenClassrooms](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/42867-introduction-aux-scripts-shell)
- [Bash scripting How To](http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html#toc)
- https://github.com/awesome-lists/awesome-bash

### Pour aller plus loin

- Automatiser les tâches que vous faites le plus souvent avec des scripts
- Re-créer Docker avec Bash (pour comprendre): https://github.com/p8952/bocker

## Git

- [ ] Créer un dépôt local
- [ ] Créer un dépôt distant sur GitHub et GitLab
- [ ] Lier un dépôt local à un dépôt distant
- [ ] Faire un commit
- [ ] Synchroniser un dépôt local avec un dépôt distant
- [ ] Le fonctionnement des branches et comment passer de l'une à l'autre
- [ ] Gérer les conflits
- [ ] Faire un tag, créer une release
- [ ] Les merge requests
- [ ] Les forks
- [ ] Gérer correctement ses branches git (Feature Branch Worflow)

### Ressources

- https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud
- https://gitexplorer.com/
- https://try.github.io
- https://openclassrooms.com/courses/gerer-son-code-avec-git-et-github
- https://git-scm.com/docs
- http://ohshitgit.com/ 

### Pour aller plus loin

- Utiliser des Git Hooks dans un projet
- Comprendre la différence entre `merge` et `rebase`
- Creuser le sujet du GitOps (Cloud)
- Comprendre la problématique du retour en arrière avec git

## Internet

- [ ] Le modèle TCP/IP
- [ ] Fonctionnement d'un réseau IP
- [ ] Le protocole TCP et les ports
- [ ] Le protocole DNS
- [ ] Utilités d'un (reverse-)proxy
- [ ] Différence entre un switch et un routeur
- [ ] Fonctionnement d'un firewall
- [ ] Différence entre LAN, WAN et WLAN
- [ ] Architecture globale d'Internet avec réseaux privées et publics
- [ ] Configuration minimale pour communiquer

### Ressources

- https://openclassrooms.com/fr/courses/857447-apprenez-le-fonctionnement-des-reseaux-tcp-ip
- https://github.com/clowwindy/Awesome-Networking

### Pour aller plus loin

- Utiliser Wireshark pour regarder ce qui est envoyé/reçu par son système
- Comprendre comment fonctionne DHCP
- Configuré un réseau IPv4 chez soi manuellement
- Comprendre les apports de IPv6
- Découvrir IPFS

## Les API REST

- [ ] Le protocole HTTP en détails (types de requêtes et réponses, headers)
- [ ] Le JSON et le YAML
- [ ] Le concept de CRUD
- [ ] Le principe des URL/fonctions des API REST
- [ ] Utiliser Postman
- [ ] Comprendre une documentation Swagger
- [ ] Les headers CORS (Cross-Origin Resource Sharing)

### Ressources

- https://openclassrooms.com/fr/courses/3449001-utilisez-des-api-rest-dans-vos-projets-web/3501901-pourquoi-rest
- https://github.com/marmelab/awesome-rest
- https://github.com/Kikobeats/awesome-api

### Pour aller plus loin

- Développer une API REST dans le langage que vous vouler
- Ajouter une authentification sur son API avec les tokens JWT
- Utiliser l'API REST de Wordpress pour développer un site plus élastique

## Docker

- [ ] Le fonctionnement de la virtualisation et son utilité
- [ ] Lancer un container Docker
- [ ] Récupérer une image depuis Docker Hub
- [ ] Les tags d'image Docker
- [ ] Créer une image Docker (`Dockerfile`)
- [ ] Mapper un volume à un container
- [ ] Mapper un port à un container
- [ ] Fournir des variables d'environnement à un container
- [ ] Différence entre le mode intéractif et le mode démon
- [ ] Lier deux containers ensemble
- [ ] Publier ses images sur Docker Hub
- [ ] Docker-Compose pour gérer facilement plusieurs containers

### Ressources

- [Getting Started with Docker](https://www.docker.com/get-started)
- [Play with Docker](https://www.docker.com/play-with-docker)
- [Documentation officielle](https://docs.docker.com/)
- https://github.com/veggiemonk/awesome-docker
- https://openclassrooms.com/fr/courses/2035766-optimisez-votre-deploiement-en-creant-des-conteneurs-avec-docker

### Pour aller plus loin

- Créer un Dockerfile pour vos projets de développement
- Optimiser un Dockerfile grâce au build multi-stages
- Utiliser NGINX pour diviser le trafic entre plusieurs containers
- Déployer une application dans un container Docker sur un hébergeur Cloud (DigitalOcean, Hidora...)

## Automatisation

- [ ] Le CI/CD et le concept de pipeline
- [ ] La différence entre jobs et stages
- [ ] Les outils d'automatisation les plus utilisés
- [ ] Utiliser Docker pour faciliter les déploiements
- [ ] Le concept d'*Infrastructure as Code*
- [ ] Créer un pipeline simple avec GitLab permettant de déployer un service à chaque push

### Ressources

- [L'approche CI/CD](https://www.redhat.com/fr/topics/devops/what-is-ci-cd)
- [Documentation pour les pipelines GitLab](https://docs.gitlab.com/ee/ci/yaml/README.html)

### Pour aller plus loin

- Utiliser GitLab dans vos projets pour effectuer des actions sur votre code automatiquement (testing, lint, déploiement, analyse, notifications...)
- Installer votre projet serveur git comme GitLab (lourd) ou Gitea (léger)
- Creuser le concept de DevOps
